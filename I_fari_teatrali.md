# I fari teatrali

Anticamente gli spettacoli venivano realizzati e pensati per spazi aperti. In Grecia - Epidauro, ad esempio - ma anche in Italia - Taormina, ad esempio - sono molti gli anfiteatri che è possibile visitare. Il fatto di rappresentare all'aperto e di giorno rendeva superfluo l'utilizzo di illuminazione artificiale. Gli spettacoli duravano molte ore e il pubblico andava e veniva dagli spalti.

Ad un certo punto della storia si è cominciato a rappresentare gli spettacoli al chiuso (o al semi chiuso, come al Globe Theatre, in cui recitavano Shakespeare e compagni). La necessità di illuminare le scene era evidente, ma disponendo solo di lampade ad olio e candele le difficoltà tecniche erano molte. Inoltre tutte queste fiamme all'interno di strutture costruite prevalentemente in legno rappresentavano un serio pericolo. Non avendo altro a disposizione, comunque, si accettava il rischio e si illuminavano le scene con il fuoco.

Sul bordo esterno del proscenio era situata la ribalta, da cui il detto "luci della ribalta". La ribalta è una piccola struttura, appunto ribaltabile, che una volta aperta consente il posizionamento delle lampade. Le lampade restano nascoste al pubblico dalla struttura della ribalta una volta aperta.

Per consentire adeguata illuminazione, in ribalta e non solo - si utilizzavano altri punti strategici, dietro alcune scenografie od oggetti, ad esempio - erano posizionate sorgenti luminose. Al fine di direzionare la luce, dietro alla sorgente si posizionava una lamina di materiale riflettente.

Fortunatamente oggi disponiamo di tecnologia adeguata per illuminare qualsiasi cosa. Purtroppo, molto spesso, l'utilizzo di questa tecnologia è piuttosto casuale. Occorre quindi fare luce su ciò che è opportuno immaginare già  in fase di progettazione. Ecco quindi un elenco di tipologie di fari a disposizione. Ciascun faro ha una prerogativa propria e un utilizzo dedicato. Il più importante si chiama PC, o Spotlight, o più semplicemente "teatrale", che è il faro più utilizzato; poi ci sono i fari sagomatori, piuttosto costosi, ma che consentono tagli di luce molto efficaci.

Per illuminare i fondali si utilizzano i Domino (anticamente detti "ciclorama"), che non consentono nessuna limitazione della luce emessa. Poi vi sono le teste mobili (o teste rotanti, o brandeggiabili, o motorizzati), tipi di fari che sono dotati di un motore che ne consente lo spostamento e più in generale la movimentazione senza dover "puntare".

Infine vi sono i PAR e PARNEL. I primi sono fari molto semplici, che fanno una luce molto "volumetrica": sono quelli che troviamo nei villaggi turistici, per intenderci (sono molto economici), i secondi sono leggermente più evoluti.

La tecnologia a led sta rivoluzionando il mondo dell'illuminotecnica, perché con questo tipo di illuminazione è possibile cambiare colori senza ricorso a gelatine e filtri. Le teste mobili, oggi, funzionano solamente a led.

Da: [https://www.associazioneteatrostudio.com/index.php/progettare-un-teatro?view=article&id=6&catid=2](https://www.associazioneteatrostudio.com/index.php/progettare-un-teatro?view=article&id=6&catid=2)
